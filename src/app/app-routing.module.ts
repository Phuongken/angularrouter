import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DetailComponent} from './detail/detail.component';
import {ListEmployeeComponent} from './list-employee/list-employee.component';
import { CommonModule } from '@angular/common';
const routes: Routes = [
   {path: 'detail', component:DetailComponent},
   {path: 'listEmployee', component:ListEmployeeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
