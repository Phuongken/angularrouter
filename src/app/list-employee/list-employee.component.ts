import { Component, OnInit } from '@angular/core';
import {ListEmployee} from '../ListEmployee';
@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css']
})
export class ListEmployeeComponent implements OnInit {
  list = ListEmployee;
  constructor() { }

  ngOnInit() {
  }

} 

